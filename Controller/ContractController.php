<?php

namespace Controller;

use Model\ContractRepository;

class ContractController extends BaseController
{

    public function readAction(){
        $contractId = isset($_GET['contractId']) ? $_GET['contractId'] : null;
        $contractRepository = new ContractRepository();

        if($contractId){
            $vars['contract'] = $contractRepository->getById($contractId);
        }else{
            $vars['contracts']= $contractRepository->getAll();
        }

        $this->render('./View/Contract/read.view.php', $vars);
    }


}