<?php

namespace Controller;

use Model\User;
use Model\UserRepository;


class UserController extends BaseController
{
    public function createAction(){
        if ($_POST) {

        $user = new User();
        $user->setFirstName($_POST['firstName']);
        $user->setLastName($_POST['lastName']);
        $user->setUsername($_POST['username']);
        $user->setEmail($_POST['email']);
        $user->setPassword($_POST['password']);

        $userRepository = new UserRepository();
        $userRepository->saveUser($user);
        echo "<pre>";
        var_dump($user);
        die('kraj');
    }

        $this->render('./View/User/create.view.php', $vars);
    }

    public function updateAction(){

    }


    public function readAction(){
        $userId = isset($_GET['userId']) ? $_GET['userId'] : null;
        $userRepository = new UserRepository();
        if ($userId) {
            $vars['user'] = $userRepository->getById($userId);
        } else {
            $vars['users'] = $userRepository->getAll();
        }

        $this->render('./View/User/read.view.php', $vars);
    }

    public function deleteAction(){

    }


}