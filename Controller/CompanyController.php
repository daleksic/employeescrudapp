<?php

namespace Controller;

use Model\CompanyRepository;

class CompanyController extends BaseController
{
    public function createAction(){}

    public function updateAction(){}

    public function readAction(){
        $companyId = isset($_GET['companyId']) ? $_GET['companyId'] : null;
        $companyRepository = new CompanyRepository();

        if($companyId){
            $vars['company'] = $companyRepository->getById($companyId);
        }else{
            $vars['companies'] = $companyRepository->getAll();
        }

        $this->render('./View/Company/read.view.php', $vars);
    }
}