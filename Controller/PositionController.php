<?php

namespace Controller;


use Model\PositionRepository;

class PositionController extends BaseController
{
    public function createAction(){}

    public function updateAction(){}

    public function readAction(){
        $positionId = isset($_GET['positionId']) ? $_GET['positionId'] : null;
        $positionRepository = new PositionRepository();

        if($positionId){
            $vars['position'] = $positionRepository->getById($positionId);
        }else{
            $vars['positions'] = $positionRepository->getAll();
        }

        $this->render('./View/Position/read.view.php', $vars);
    }

    public function deleteAction(){}

}