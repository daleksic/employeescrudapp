<?php
namespace Controller;

use Model\RegisteredActivityRepository;

class RegisteredActivityController extends BaseController
{
    public function createAction(){}

    public function updateAction(){}

    public function readAction(){
        $regActId = isset($_GET['regActId']) ? $_GET['regActId'] : null;
        $regActRepository = new RegisteredActivityRepository();

        if($regActId){
            $vars['regAct'] = $regActRepository->getById($regActId);
        }else{
            $vars['regActs'] = $regActRepository->getAll();
        }

        $this->render('./View/RegisteredActivity/read.view.php', $vars);
    }

    public function deleteAction(){}

}