<h1>Contracts</h1>

<?php
if (isset($vars['contracts'])){
    $contracts = $vars['contracts'];
?>
    <table>
        <thead>
        <tr>
            <th>
                ID
            </th>
            <th>
                Path
            </th>
            <th>
                Name
            </th>
            <th>
                Created At
            </th>
        </tr>
        </thead>
        <tbody>
            <?php
            foreach ($contracts as $contract){
            ?>
            <tr>
                <td>
                    <?php echo $contract->getId(); ?>
                </td>
                <td>
                    <?php echo $contract->getPath(); ?>
                </td>
                <td>
                    <?php echo $contract->getName(); ?>
                </td>
                <td>
                    <?php echo $contract->getCreatedAt(); ?>
                </td>

                <td>
                    <a href="?contractId=<?php echo $contract->getId(); ?>">READ</a>
                    <a href="#">UPDATE</a>
                    <a href="#">DELETE</a>
                </td>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
    <?php
} else if (isset($vars['contract'])) {
    $contract = $vars['contract'];
    ?>
    <h3>Single user profile</h3>
    <p>
        Id is: <strong><?php echo $contract->getId(); ?></strong><br>
        Path is: <strong><?php echo $contract->getPath(); ?></strong><br>
        Name is: <strong><?php echo $contract->getName(); ?></strong><br>
        Created At is: <strong><?php echo $contract->getCreatedAt(); ?></strong><br>

    </p>
    <?php
}