<h1>Employees</h1>


<?php
if (isset($vars['employees'])) {
    $employees = $vars['employees'];
    ?>
    <table>
        <thead>
        <tr>
            <th>
                ID
            </th>
            <th>
                First Name
            </th>
            <th>
                Last Name
            </th>
            <th>
                Email
            </th>
            <th>
                Date Of Birth
            </th>
            <th>
                Company
            </th>
            <th>
                Position
            </th>
            <th>
                Contract
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($employees as $employee) {
            ?>
            <tr>
                <td>
                    <?php echo $employee->getId(); ?>
                </td>
                <td>
                    <?php echo $employee->getFirstName(); ?>
                </td>
                <td>
                    <?php echo $employee->getLastName(); ?>
                </td>
                <td>
                    <?php echo $employee->getEmail(); ?>
                </td>
                <td>
                    <?php echo $employee->getDateOfBirth(); ?>
                </td>
                <td>
                    <?php echo $employee = "Company"; ?>
                </td>
                <td>
                    <?php echo $employee = "Position"; ?>
                </td>
                <td>
                    <?php echo $employee = "Contract"; ?>
                </td>

                <td>
                    <a href="?employeeId=<?php echo $employee->getId(); ?>">READ</a>
                    <a href="#">UPDATE</a>
                    <a href="#">DELETE</a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <?php
} else if (isset($vars['employee'])) {
    $employee = $vars['employee'];
    ?>
    <h3>Single Employee profile</h3>
    <p>
        Id: <strong><?php echo $employee->getId(); ?></strong><br>
        First Name: <strong><?php echo $employee->getFirstName(); ?></strong><br>
        Last Name: <strong><?php echo $employee->getLastName(); ?></strong><br>
        Email: <strong><?php echo $employee->getEmail(); ?></strong><br>
        Date Of Birth: <strong><?php echo $employee->getDateOfBirth(); ?></strong><br>
        Company: <strong><?php echo $employee = "Company"; ?></strong><br>
        Position: <strong><?php echo $employee= "Position"; ?></strong><br>
        Contract: <strong><?php echo $employee= "Contract"; ?></strong><br>
    </p>
    <?php
}