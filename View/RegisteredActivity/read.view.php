<h1>Registered Activities</h1>

<?php
if (isset($vars['regActs'])){
    $regActs = $vars['regActs'];
    ?>
    <table>
        <thead>
        <tr>
            <th>
                ID
            </th>
            <th>
                Name
            </th>
            <th>
                Activity Number
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($regActs as $regAct){
            ?>
            <tr>
                <td>
                    <?php echo $regAct->getId(); ?>
                </td>
                <td>
                    <?php echo $regAct->getName(); ?>
                </td>
                <td>
                    <?php echo $regAct->getActivityNumber(); ?>
                </td>

                <td>
                    <a href="?regActId=<?php echo $regAct->getId(); ?>">READ</a>
                    <a href="#">UPDATE</a>
                    <a href="#">DELETE</a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <?php
} else if (isset($vars['regAct'])) {
    $regAct = $vars['regAct'];
    ?>
    <h3>Single position profile</h3>
    <p>
        Id is: <strong><?php echo $regAct->getId(); ?></strong><br>
        Name is: <strong><?php echo $regAct->getName(); ?></strong><br>
        Activity Number is: <strong><?php echo $regAct->getActivityNumber(); ?></strong><br>


    </p>
    <?php
}
