<h1>Companies</h1>


<?php
if (isset($vars['companies'])) {
    $companies = $vars['companies'];
    ?>
    <table>
        <thead>
        <tr>
            <th>
                ID
            </th>
            <th>
                Name
            </th>
            <th>
                Tax Number
            </th>
            <th>
                Registered At
            </th>

        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($companies as $company) {
            ?>
            <tr>
                <td>
                    <?php echo $company->getId(); ?>
                </td>
                <td>
                    <?php echo $company->getName(); ?>
                </td>
                <td>
                    <?php echo $company->getTaxNumber(); ?>
                </td>
                <td>
                    <?php echo $company->getRegisteredAt(); ?>
                </td>

                <td>
                    <a href="?companyId=<?php echo $company->getId(); ?>">READ</a>
                    <a href="#">UPDATE</a>
                    <a href="#">DELETE</a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <?php
} else if (isset($vars['company'])) {
    $company = $vars['company'];
    ?>
    <h3>Single user profile</h3>
    <p>
        Id is: <strong><?php echo $company->getId(); ?></strong><br>
        Name of Company is: <strong><?php echo $company->getName(); ?></strong><br>
        Tax Number is: <strong><?php echo $company->getTaxNumber(); ?></strong><br>
        Registered At is: <strong><?php echo $company->getRegisteredAt(); ?></strong><br>

    </p>
    <?php
}