<h1>Positions</h1>

<?php
if (isset($vars['positions'])){
    $positions = $vars['positions'];
    ?>
    <table>
        <thead>
        <tr>
            <th>
                ID
            </th>
            <th>
                Name
            </th>
            <th>
                Registered Activity
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($positions as $position){
            ?>
            <tr>
                <td>
                    <?php echo $position->getId(); ?>
                </td>
                <td>
                    <?php echo $position->getName(); ?>
                </td>
                <td>
                    <?php echo $position = "Registered Activity"; ?>
                </td>

                <td>
                    <a href="?positionId=<?php echo $position->getId(); ?>">READ</a>
                    <a href="#">UPDATE</a>
                    <a href="#">DELETE</a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <?php
} else if (isset($vars['position'])) {
    $position = $vars['position'];
    ?>
    <h3>Single position profile</h3>
    <p>
        Id is: <strong><?php echo $position->getId(); ?></strong><br>
        Name is: <strong><?php echo $position->getName(); ?></strong><br>
        Registered Activity is: <strong><?php echo $position= "Reg. Act"; ?></strong><br>


    </p>
    <?php
}